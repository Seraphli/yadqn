import tensorflow as tf
import utility.tf_layer as layer
from utility.epsilon import LinearAnnealEpsilon
from utility.exp_replay import ExpReplay
from utility.utility import get_path


class DQfD(object):
    def __init__(self, sess, env, config, logger=None):
        self._sess = sess
        self._env = env
        self._config = config
        self._logger = logger
        self._action_n = self._env.action_space.n
        self.setup()

    def setup(self):
        self._def_algorithm()
        self._def_step_count()
        self._def_tf_sl()
        self._def_agent()

    def _def_algorithm(self):
        self._def_algorithm_specify()
        self._def_algorithm_extend()

    def _def_algorithm_specify(self):
        self.expert_sampling_ratio = self._config['Algorithm']['ExpertSamplingRatio']
        self.lambda_1 = self._config['Algorithm']['Lambda_1']
        self.lambda_2 = self._config['Algorithm']['Lambda_2']
        self.expert_margin = self._config['Algorithm']['ExpertMargin']

    def _def_algorithm_extend(self):
        self.gamma = self._config['Gamma']
        self.lr = self._config['LR']
        self.momentum = self._config['Momentum']
        self.epsilon_decay = LinearAnnealEpsilon(self._config['Epsilon']['Initial'],
                                                 self._config['Epsilon']['Final'],
                                                 self._config['Epsilon']['Total'])
        self.epsilon = self.epsilon_decay.step_op
        self.memory = ExpReplay(self._config['ReplaySize'])
        self.minibatch_size = self._config['MiniBatchSize']
        self.target_net_update_frequency = self._config['TargetNetUpdateFrequency']
        self.update_frequency = self._config['UpdateFrequency']
        self.save_frequency = self._config['SaveFrequency']
        self.log_interval = self._config['LogInterval']

    def _def_step_count(self):
        self.step_count = 0
        self.step_count_tensor = tf.get_variable('step_count', [], tf.int64, tf.zeros_initializer())
        self.step_count_inc = tf.assign(self.step_count_tensor, self.step_count_tensor + 1)

    def _def_tf_sl(self):
        self.saver = tf.train.Saver()
        self.save_path = get_path('models/' + self._env.spec.id)

    def _def_agent(self):
        self._def_network()
        self._def_update_params()
        self._def_action()

    def _def_network(self):
        w_init, b_init = tf.random_normal_initializer(0., 0.3), tf.constant_initializer(0.1)

        def build_network(x, c_names):
            c1 = layer.conv_layer(x, [8, 8, 4, 32], [32], w_init, b_init, 4, 'c1', c_names)
            c2 = layer.conv_layer(c1, [4, 4, 32, 64], [64], w_init, b_init, 2, 'c2', c_names)
            c3 = layer.conv_layer(c2, [3, 3, 64, 64], [64], w_init, b_init, 1, 'c3', c_names)
            fl = tf.contrib.layers.flatten(c3)
            fl_size = fl.shape[1].value
            fc1 = layer.fc_relu_layer(fl, [fl_size, 512], [512], w_init, b_init, 'fc1', c_names)
            fc2 = layer.fc_layer(fc1, [512, self._action_n], [self._action_n], w_init, b_init, 'fc2', c_names)
            return fc2

        self.s = tf.placeholder(tf.float32, [None, 84, 84, 4], name='s')
        self.s_ = tf.placeholder(tf.float32, [None, 84, 84, 4], name='s_')
        self.r = tf.placeholder(tf.float32, [None, ], name='r')
        self.a = tf.placeholder(tf.int32, [None, ], name='a')
        self.t = tf.placeholder(tf.uint8, [None, ], name='t')
        # True if sample from expert
        self.e = tf.placeholder(tf.bool, [None, ], name='e')

        with tf.variable_scope('online_net'):
            c_names = ['online_net_params', tf.GraphKeys.GLOBAL_VARIABLES]
            self.q_s_a = build_network(self.s, c_names)

        with tf.variable_scope('online_net', reuse=True):
            c_names = ['online_net_params', tf.GraphKeys.GLOBAL_VARIABLES]
            self.q_s_1_a = build_network(self.s_, c_names)

        self.o_params = tf.get_collection('online_net_params')

        with tf.variable_scope('target_net'):
            c_names = ['target_net_params', tf.GraphKeys.GLOBAL_VARIABLES]
            self.q_t_s_1_a = build_network(self.s_, c_names)

        self.t_params = tf.get_collection('target_net_params')

        with tf.variable_scope('q_target'):
            self.q_target = self.r + (1. - tf.cast(self.t, tf.float32)) * self.gamma * self.q_t_s_1_a[
                tf.arg_max(self.q_s_1_a, dimension=1)]

        with tf.variable_scope('loss'):
            a_one_hot = tf.one_hot(self.a, depth=self._action_n, dtype=tf.float32)
            self.net_q_value = tf.reduce_sum(self.q_s_a * a_one_hot, axis=1)
            J_DQ = tf.reduce_mean(tf.squared_difference(self.q_target, self.net_q_value), name='J_DQ')
            J_E = tf.reduce_mean(tf.reduce_max(self.q_s_a, axis=1) +
                                 self.expert_margin * tf.cast(tf.equal(tf.arg_max(self.q_s_a, dimension=1), self.a),
                                                              tf.float32) -
                                 [self.q_s_a[i, j] for i, j in enumerate(self.a)], name='J_E')
            J_L2 = tf.reduce_sum([tf.nn.l2_loss(w) for w in self.o_params], name='J_L2')
            self.loss = tf.cond(self.e, J_DQ + self.lambda_1 * J_E + self.lambda_2 * J_L2, J_DQ + self.lambda_2 * J_L2)

        with tf.variable_scope('train'):
            self._train_op = tf.train.RMSPropOptimizer(self.lr, momentum=self.momentum).minimize(self.loss)

    def _def_update_params(self):
        self.replace_params = [tf.assign(t, e) for t, e in zip(self.t_params, self.o_params)]

    def _def_action(self):
        random_action = tf.random_uniform((), 0, self._action_n, tf.int64)
        should_explore = tf.random_uniform((), 0, 1) < self.epsilon
        best_action = tf.arg_max(self.q_s_a, dimension=1)
        self.action = tf.cond(should_explore, lambda: random_action, lambda: best_action)

    def load_session(self):
        checkpoint = tf.train.get_checkpoint_state(self.save_path)
        if checkpoint and checkpoint.model_checkpoint_path:
            self.saver.restore(self._sess, checkpoint.model_checkpoint_path)
            if self._logger != None:
                self._logger.info("Successfully loaded: %s" % checkpoint.model_checkpoint_path)
            return True
        else:
            if self._logger != None:
                self._logger.info('Could not find old network weights')
            return False

    def save_session(self):
        if self.step_count % self.save_frequency == 0:
            self.saver.save(self._sess, self.save_path + '/DQfD/model', global_step=self.step_count)

    def update_param(self):
        if self.step_count % self.target_net_update_frequency == 0:
            if self._logger != None:
                self._logger.debug('Replace network parameters')
            self._sess.run(self.replace_params)

    def train(self):
        if self.step_count % self.update_frequency != 0:
            return
        minibatch = self.memory.batch(self.minibatch_size)
        s, a, r, t, s_ = [[i[j] for i in minibatch] for j in range(4)]
        _, loss = self._sess.run(
            [self._train_op, self.loss],
            feed_dict={
                self.s: s,
                self.a: a,
                self.r: r,
                self.t: t,
                self.s_: s_
            })
        if self._logger != None and self.step_count % self.log_interval == 0:
            self._logger.info('Current loss: %f' % loss)

    def take_action(self, observation):
        if self._logger != None:
            self._logger.debug('Generate action')
            self._logger.debug('Epsilon: %f' % self.epsilon_decay.run_step(self.step_count))
        return self._sess.run(self.action, feed_dict={self.s: observation, self.epsilon_decay.step: self.step_count})

    def step(self, observation):
        self._sess.run(self.step_count_inc)
        self.step_count = self._sess.run(self.step_count_tensor)
        if self._logger != None and self.step_count % self.log_interval == 0:
            self._logger.info('Current step: %d' % self.step_count)
        self.update_param()
        self.train()
        self.save_session()
        return self.take_action(observation)

    def store_transition(self, s, a, r, t):
        self.memory.append((s, a, r, t))
