import tensorflow as tf
from .DQN import DQN


class DoubleDQN(DQN):
    def _def_algorithm_name(self):
        self._algo_name = 'DoubleDQN'

    def _def_network(self):
        self.s = tf.placeholder(tf.uint8, [None, 84, 84, 4], name='s')
        self.s_ = tf.placeholder(tf.uint8, [None, 84, 84, 4], name='s_')
        self.r = tf.placeholder(tf.float32, [None, ], name='r')
        self.a = tf.placeholder(tf.int32, [None, ], name='a')
        self.t = tf.placeholder(tf.uint8, [None, ], name='t')

        self.q_s, self.online_w, self.online_l, self.online_vl, self.online_vw \
            = self._build_network(self.s, "online", False, True, False)
        self.q_s_, _, _, _, _ \
            = self._build_network(self.s_, "online", True, True, False)
        self.q_t_s_, self.target_w, self.target_l, self.target_vl, self.target_vw \
            = self._build_network(self.s_, "target", False, False, True)

        # This makes double q learning different from previous dqn.
        with tf.variable_scope('q_target'):
            self.q_target = self.r + (1. - tf.cast(self.t, tf.float32)) * self.gamma * tf.reduce_max(
                tf.multiply(tf.one_hot(tf.argmax(self.q_s_, axis=1), self._action_n), self.q_t_s_), axis=1)

        with tf.variable_scope('loss'):
            a_one_hot = tf.one_hot(self.a, depth=self._action_n, dtype=tf.float32)
            self.net_q_value = tf.reduce_sum(self.q_s * a_one_hot, axis=1)
            self.loss_op = tf.reduce_mean(tf.squared_difference(self.q_target, self.net_q_value), name='MSE')

        self.train_op = tf.train.RMSPropOptimizer(self.lr, decay=.95, momentum=self.momentum, epsilon=0.01) \
            .minimize(self.loss_op)
