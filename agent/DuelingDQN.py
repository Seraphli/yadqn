import tensorflow as tf
from .DoubleDQN import DoubleDQN


class DuelingDQN(DoubleDQN):
    def _def_algorithm_name(self):
        self._algo_name = 'DuelingDQN'

    def _build_network(self, x, name, reuse, trainable, visualize):
        layers_name = ["conv1", "conv2", "conv3", "flatten", "fc1_v", "fc1_a", "fc2_v", "fc2_a", "q"]
        weighted_layers_name = ["conv1", "conv2", "conv3", "fc1_v", "fc1_a", "fc2_v", "fc2_a"]
        weights = []
        layers = []
        visualize_layer = []
        visualize_weight = []
        collect_names = [name, tf.GraphKeys.GLOBAL_VARIABLES]

        y = tf.to_float(x) / 255.0
        layers.append(y)

        with tf.variable_scope(layers_name[0] + "_" + name, reuse=reuse):
            w = tf.get_variable("w", [8, 8, 4, 32], initializer=self.w_init,
                                trainable=trainable, collections=collect_names)
            b = tf.get_variable("b", [32], initializer=self.b_init,
                                trainable=trainable, collections=collect_names)
            y = tf.nn.relu(tf.nn.conv2d(y, w, strides=[1, 4, 4, 1], padding="VALID") + b)
        weights += [w, b]
        layers.append(y)

        with tf.variable_scope(layers_name[1] + "_" + name, reuse=reuse):
            w = tf.get_variable("w", [4, 4, 32, 64], initializer=self.w_init,
                                trainable=trainable, collections=collect_names)
            b = tf.get_variable("b", [64], initializer=self.b_init,
                                trainable=trainable, collections=collect_names)
            y = tf.nn.relu(tf.nn.conv2d(y, w, strides=[1, 2, 2, 1], padding="VALID") + b)
        weights += [w, b]
        layers.append(y)

        with tf.variable_scope(layers_name[2] + "_" + name, reuse=reuse):
            w = tf.get_variable("w", [3, 3, 64, 64], initializer=self.w_init,
                                trainable=trainable, collections=collect_names)
            b = tf.get_variable("b", [64], initializer=self.b_init,
                                trainable=trainable, collections=collect_names)
            y = tf.nn.relu(tf.nn.conv2d(y, w, strides=[1, 1, 1, 1], padding="VALID") + b)
        weights += [w, b]
        layers.append(y)

        with tf.variable_scope(layers_name[3] + "_" + name, reuse=reuse):
            fl = tf.reshape(y, [-1, 7 * 7 * 64], name="flatten")

        # This makes dueling network different from previous dqn.
        with tf.variable_scope(layers_name[4] + "_" + name, reuse=reuse):
            w = tf.get_variable("w", [7 * 7 * 64, 512], initializer=self.w_init,
                                trainable=trainable, collections=collect_names)
            b = tf.get_variable("b", [512], initializer=self.b_init,
                                trainable=trainable, collections=collect_names)
            fc1_v = tf.nn.relu(tf.matmul(fl, w) + b)
        weights += [w, b]
        layers.append(fc1_v)

        with tf.variable_scope(layers_name[5] + "_" + name, reuse=reuse):
            w = tf.get_variable("w", [7 * 7 * 64, 512], initializer=self.w_init,
                                trainable=trainable, collections=collect_names)
            b = tf.get_variable("b", [512], initializer=self.b_init,
                                trainable=trainable, collections=collect_names)
            fc1_a = tf.nn.relu(tf.matmul(fl, w) + b)
        weights += [w, b]
        layers.append(fc1_a)

        with tf.variable_scope(layers_name[6] + "_" + name, reuse=reuse):
            w = tf.get_variable("w", [512, 1], initializer=self.w_init,
                                trainable=trainable, collections=collect_names)
            b = tf.get_variable("b", [1], initializer=self.b_init,
                                trainable=trainable, collections=collect_names)
            fc2_v = tf.matmul(fc1_v, w) + b
        weights += [w, b]
        layers.append(fc2_v)

        with tf.variable_scope(layers_name[7] + "_" + name, reuse=reuse):
            w = tf.get_variable("w", [512, self._action_n], initializer=self.w_init,
                                trainable=trainable, collections=collect_names)
            b = tf.get_variable("b", [self._action_n], initializer=self.b_init,
                                trainable=trainable, collections=collect_names)
            fc2_a = tf.matmul(fc1_a, w) + b
        weights += [w, b]
        layers.append(fc2_a)

        with tf.variable_scope(layers_name[8] + "_" + name, reuse=reuse):
            y = fc2_v + (fc2_a - tf.stack([tf.reduce_mean(fc2_a, axis=1)] * self._action_n, axis=1))
        layers.append(y)

        if visualize:
            with tf.variable_scope("visualize_" + name, reuse=reuse):
                visualize_layer.append(self._visualize_conv_layer(layers[0], (84, 84, 4, 1, 4)))
                visualize_layer.append(self._visualize_conv_layer(layers[1], (20, 20, 32, 8, 4)))
                visualize_layer.append(self._visualize_conv_layer(layers[2], (9, 9, 64, 8, 8)))
                visualize_layer.append(self._visualize_conv_layer(layers[3], (7, 7, 64, 8, 8)))
                visualize_layer.append(self._visualize_fc_layer(layers[4], (16, 32)))
                visualize_layer.append(self._visualize_fc_layer(layers[5], (16, 32)))
                visualize_layer.append(self._visualize_fc_layer(layers[6], (1, 1)))
                visualize_layer.append(self._visualize_fc_layer(layers[7], (1, self._action_n)))

            for i in range(len(weighted_layers_name)):
                visualize_weight.append(tf.summary.histogram(weighted_layers_name[i] + "/w", weights[2 * i]))
                visualize_weight.append(tf.summary.histogram(weighted_layers_name[i] + "/b", weights[2 * i + 1]))
                visualize_weight.append(tf.summary.histogram(weighted_layers_name[i] + "/layer", layers[i + 1]))

        return y, weights, layers, visualize_layer, visualize_weight
