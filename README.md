# Yet Another Deep Q Network

## Why build another repository?

When I look at repositories related to DQN in Github, they seperate codes into tiny parts, codes without so much explanatoin. I can not really see the whole picture, neither can I rewrite some part of code and do some improvements. So I decide to write my own repository about DQN, which works as a benchmark and can compare different algorithms.

## Aims

1. Test DQN in this repo
2. Generate benchmark for algorithm
3. Test my own algorithm and compare with them

## Install

Recommend: use virtual env and pycharm

Notice: If you are not using PyCharm to open project folder,
you need to add project folder path into `PYTHONPATH`.

### Prerequisition

1. sudo apt-get install cmake swig zlib1g-dev
2. pip install gym[all]
3. pip install tensorflow-gpu

## Run

Checkout `code` folder.

## Project Structure

- code: Actually codes to run.
- agent: Codes about agent.
- test: Test cases go here.
- utility: Codes not about reinforcement learning.

## Reference

- https://github.com/bbitmaster/ale_python_interface
- https://github.com/devsisters/DQN-tensorflow
- https://github.com/yenchenlin/DeepLearningFlappyBird