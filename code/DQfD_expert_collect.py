from utility.interface import HumanInterface


def demo_collect(game, size):
    demo_count = 0
    hi = HumanInterface(game)
    while demo_count < size:
        if hi.play(record=True):
            break
        demo_count += 1


if __name__ == '__main__':
    demo_collect('Breakout', 1)
