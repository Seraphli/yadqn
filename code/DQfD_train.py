import gym
import tensorflow as tf
from utility.preprocess import DQNPrep
from agent.DQfD import DQfD
from utility.utility import init_logger, load_config


def train(cfg='DQfD.yml'):
    config = load_config(cfg)
    logger = init_logger(config['Train']['LoggerName'])
    logger.info('New session start')
    env_name = config['Train']['Game'] + '-v0'
    logger.debug('Environment: %s' % env_name)
    env = gym.make(env_name)
    with tf.Session() as sess:
        # Initialize
        p = DQNPrep(sess)
        p.setup()
        agent = DQfD(sess, env, config['Agent'], logger)
        agent.setup()
        init = tf.global_variables_initializer()
        sess.graph.finalize()
        if not agent.load_session():
            sess.run(init)

        for _ in range(config['Train']['Episode']):
            logger.info('Game %d start' % _)
            observation = env.reset()
            observation = p.initialize(observation)
            score = 0
            while True:
                action = agent.step(observation)
                logger.debug('Action: %d' % action)
                observation_, reward, done, info = env.step(action)
                score += reward
                logger.debug('Reward: %d' % reward)
                observation_ = p.process(observation_)
                agent.store_transition(observation, action, reward, observation_)
                observation = observation_
                if done:
                    break
            if _ % config['Train']['LogInterval'] == 0:
                logger.info('Score: %d' % score)


if __name__ == '__main__':
    import sys

    usage = """
    Usage: DQfD_train.py [DQfD.yml]
    """
    if len(sys.argv) > 2:
        print(usage)
    elif len(sys.argv) == 2:
        train(sys.argv[1])
    else:
        train()
