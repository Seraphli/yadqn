import numpy as np
from tqdm import tqdm
from utility.exp_replay import Memory
from utility.env_pool import EnvPool
from utility.preprocess import DQNPrep

DQN_REPLAY_SHAPE = (4,
                    ((84, 84, 4), np.uint8),
                    ((), np.uint8),
                    ((), np.int32),
                    ((), np.bool))
memory = Memory(100000, DQN_REPLAY_SHAPE)

ep = EnvPool('Breakout-v0', 16, DQNPrep())
total_step = 0
step = 50000
with tqdm(total=step, desc="Random") as t:
    ep.reset()
    while total_step < step:
        result = ep.random()
        for r in result:
            done, transition, frame, score = r
            memory.append(transition)
            total_step += 1
            t.update()
            if total_step == step:
                break
memory.sample_enqueue(32)
for _ in range(20):
    total_step = 0
    step = 50000
    with tqdm(total=step, desc="Train") as t:
        ep.reset()
        while total_step < step:
            ep.before_step()
            _s, _a, _r, _t, _s_ = memory.sample()
            actions = [1, 3, 4, 5] * 4
            result = ep.step(actions)
            for r in result:
                done, transition, frame, score = r
                memory.append(transition)
                total_step += 1
                t.update()
                if total_step == step:
                    break
    total_step = 0
    step = 50000
    with tqdm(total=step, desc="Test") as t:
        ep.reset()
        while total_step < step:
            ep.before_step()
            actions = [1, 3, 4, 5] * 4
            result = ep.step(actions)
            for r in result:
                done, transition, frame, score = r
                memory.append(transition)
                total_step += 1
                t.update()
                if total_step == step:
                    break
ep.close()
