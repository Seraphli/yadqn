import gym
import tensorflow as tf
from agent.DuelingDQN import DuelingDQN
from utility.utility import init_logger, load_config


def game(env, agent, phase, step):
    n_games = 0
    min_score = .0
    max_score = .0
    total_score = .0
    total_step = 0
    while total_step < step:
        n_games += 1
        score = 0
        lives = -1
        observation = env.reset()
        while True:
            if phase == "Random":
                observation = agent.preprocess(observation)
                action = env.action_space.sample()
            elif phase == "Train":
                observation = agent.preprocess(observation)
                action = agent.train([observation])
            else:
                observation = agent.preprocess(observation)
                action = agent.eval([observation])
            observation_, reward, done, info = env.step(action)
            if lives == -1:
                lives = info["ale.lives"]
                t = False
            elif lives > info["ale.lives"]:
                t = True
            else:
                t = False
            agent.store_transition(observation, action, reward, t)
            score += reward
            observation = observation_
            total_step += 1
            if done:
                break
        min_score = min(score, min_score)
        max_score = max(score, max_score)
        total_score += score
    average_score = total_score / n_games

    return n_games, total_step, average_score, min_score, max_score


def run(cfg='DuelingDQN.yml'):
    config = load_config(cfg)
    logger = init_logger(config["Train"]["LoggerName"])
    logger.info('New session start')
    env_name = config["Train"]["Game"]
    logger.debug('Environment: %s' % env_name)
    env = gym.make(env_name)
    with tf.Session() as sess:
        # Initialize
        agent = DuelingDQN(sess, env, config["Agent"], logger)
        init = tf.global_variables_initializer()
        sess.graph.finalize()

        if not agent.load_session():
            sess.run(init)

        n_games, total_step, average_score, min_score, max_score \
            = game(env, agent, "Random", config["Agent"]["Algorithm"]["ReplayStartSize"])
        logger.info('PHASE: %s, N: %d, STEP: %d, AVG: %f, MIN: %d, MAX: %d' %
                    ("Random", n_games, total_step, average_score, min_score, max_score))
        for _ in range(config["Train"]["Episode"]):
            n_games, total_step, average_score, min_score, max_score \
                = game(env, agent, "Train", config["Train"]["TrainStep"])
            logger.info('PHASE: %s, N: %d, STEP: %d, AVG: %f, MIN: %d, MAX: %d' %
                        ("Train", n_games, total_step, average_score, min_score, max_score))
            agent.add_summary("Train", (_, average_score, min_score, max_score))
            n_games, total_step, average_score, min_score, max_score \
                = game(env, agent, "Test", config["Train"]["TestStep"])
            logger.info('PHASE: %s, N: %d, STEP: %d, AVG: %f, MIN: %d, MAX: %d' %
                        ("Test", n_games, total_step, average_score, min_score, max_score))
            agent.add_summary("Test", (_, average_score, min_score, max_score))


if __name__ == '__main__':
    import sys

    usage = """
    Usage: DuelingDQN_train.py [DuelingDQN.yml]
    """
    if len(sys.argv) > 2:
        print(usage)
    elif len(sys.argv) == 2:
        run(sys.argv[1])
    else:
        run()
