from utility.interface import ReplayInterface
from utility.utility import get_path


def play_replay():
    ri = ReplayInterface('Breakout')
    ri.play(get_path('data') + '/replays/Breakout_392_9571.rep')


if __name__ == '__main__':
    play_replay()
