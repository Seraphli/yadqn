import unittest
from utility.utility import *
from utility.interface import *
from utility.compress import *


class TestBuffer(object):
    def __init__(self):
        self.buffer = []


class TestUtility(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_load_config(self):
        config = load_config('DQN.yml')
        self.assertEqual("DQN", config["Train"]["LoggerName"])

    def test_human_interface(self):
        hi = HumanInterface()
        hi.play()

    def test_lzma(self):
        t = TestBuffer()
        for i in range(100):
            t.buffer.append([1, 2, 3, 4, 5])
            t.buffer.append([6, 7, 8, 9, 0])
        cb = compress(t)
        db = decompress(cb)
        self.assertListEqual(t.buffer, db.buffer)

    def test_lzma_mp(self):
        t = TestBuffer()
        for i in range(100):
            t.buffer.append([1, 2, 3, 4, 5])
            t.buffer.append([6, 7, 8, 9, 0])
        cb = compress_mp(t)
        db = decompress_mp(cb)
        self.assertListEqual(t.buffer, db.buffer)
