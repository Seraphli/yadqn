import unittest
import tensorflow as tf
import gym
import numpy as np
from PIL import Image
from utility.preprocess import *


class TestPreprocessor(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        imgs = []
        env = gym.make("Breakout-v0")
        observation = env.reset()
        for _ in range(1000):
            if _ > 5:
                imgs.append(observation)
            if _ > 15:
                break
            action = env.action_space.sample()
            observation, reward, done, info = env.step(action)
            if done:
                break
        cls._imgs = imgs

    def setUp(self):
        pass

    def tearDown(self):
        tf.reset_default_graph()

    def test_max_over_image(self):
        p = MaxOverImagePrep()
        for i in range(2, 4):
            p.process(self._imgs[i])
        img = p.process(self._imgs[4])
        p.display()
        Image.fromarray(img).show()

    def test_rgb_to_gray(self):
        p = RGBToGrayPrep()
        Image.fromarray(self._imgs[6]).show()
        Image.fromarray(p.process(self._imgs[6])).show()

    def test_resize(self):
        p = ResizePrep((84, 84, 3))
        Image.fromarray(self._imgs[8]).show()
        Image.fromarray(p.process(self._imgs[8])).show()

    def test_image_dtype(self):
        p = ImageDTypePrep()
        img = self._imgs[3]
        Image.fromarray(img).show()
        img = p.process(img)
        img = p.process(img)
        Image.fromarray(img).show()
