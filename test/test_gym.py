import unittest


class TestGym(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_breakout_env(self):
        import gym
        env = gym.make("Breakout-v0")
        observation = env.reset()
        for _ in range(1000):
            env.render()
            # take a random action
            action = env.action_space.sample()
            observation, reward, done, info = env.step(action)
            if done:
                break


if __name__ == '__main__':
    unittest.main()
