import unittest
import tensorflow as tf


class TestTFQueue(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_example(self):
        q = tf.FIFOQueue(3, "float")
        init = q.enqueue_many(([0., 0., 0.],))

        x = q.dequeue()
        y = x + 1
        q_inc = q.enqueue([y])

        with tf.Session() as sess:
            sess.run(init)

            for i in range(3):
                sess.run(q_inc)

            que_len = sess.run(q.size())
            for i in range(que_len):
                self.assertEqual(sess.run(q.dequeue()), 1.0)
