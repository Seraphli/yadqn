import unittest, numpy as np
from utility.exp_replay import Memory
from tqdm import trange


class TestGym(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_memory_append(self):
        DQN_REPLAY_SHAPE = (4,
                            ((84, 84, 4), np.uint8),
                            ((), np.uint8),
                            ((), np.int32),
                            ((), np.uint8))
        memory = Memory(100000, DQN_REPLAY_SHAPE)
        s = np.ones([84, 84, 4], np.uint8)
        a = 0
        r = 0
        t = False
        for _ in trange(300000):
            memory.append((s, a, r, t))


if __name__ == '__main__':
    unittest.main()
